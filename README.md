# introduction

express support for get raw body on **POST** method, the content-type need to be `tex/plain`


# how to configure
	var app = require("express")();
	app.use(require('mg-nd-rawbody'));

# how to use
	app.post('/', function(req, res){
		res.send(req.rawBody);
	});
