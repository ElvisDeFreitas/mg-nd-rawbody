module.exports = function(req, res, next) {
  var data = '';
  var header = req.headers['content-type'] || '';
	if(req.method != 'POST' && header.indexOf('text/plain') === 0){
    req.on('data', function(chunk) { 
      data += chunk;
    });
    req.on('end', function() {
      req.rawBody = data;
      next();
    });
	}else{
		return next();
	}
}